import java.io.*;
import java.util.*;
import java.nio.file.*;


class q22
{
	public static void main(String ass[])throws Exception
	{
		Path path=Paths.get("E:/java/sheet1");
		PathMatcher matcher=FileSystems.getDefault().getPathMatcher("regex:.*(?i:java)");
		try(DirectoryStream<Path> stream=Files.newDirectoryStream(path,entry->matcher.matches(entry.getFileName())))
		{
			for(Path p:stream)
			{
				System.out.println(p.getFileName());
			}
		}		
	}
}