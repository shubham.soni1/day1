import java.io.Serializable;
import java.io.*;

class Login implements Serializable
{
	String name;
	transient String password;
	
	Login(String name , String password)
	{
		this.name=name;
		this.password=password;
	}
	
}
class Serialization
{
	public static void main(String arr[])throws Exception
	{
		Login login=new Login("shub","shub123");
		
		FileOutputStream fileoutputstream=new FileOutputStream("login.src");
		ObjectOutputStream objectoutputstream=new ObjectOutputStream(fileoutputstream);
	    objectoutputstream.writeObject(login);
		
		
		FileInputStream fileinputstream=new FileInputStream("login.src");
		ObjectInputStream objectinputstream=new ObjectInputStream(fileinputstream);
	    Login log =(Login)objectinputstream.readObject();
		System.out.println(log.name+" "+log.password);
	}
}