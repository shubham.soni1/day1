class organization
{
	public String toString()
	{
		return " Name : "+name+", Age :  "+age+", Department : "+department+", Salary : "+salary+", JoinDate : "+joindate; 
	}
	private String name;
	private String gender;
	private String department;
	private int age;
	private int salary;
	private int joindate;
	
	organization(String name,String gender,String department,int age,int salary,int joindate)
	{
		this.name=name;
		this.gender=gender;
		this.department=department;
		this.age=age;
		this.salary=salary;
		this.joindate=joindate;
	}
	
	public String name()
	{
		return name;
	}
	public String gender()
	{
		return gender;
	}
	
	public String department()
	{
		return department;
	}
	
	public int age()
	{
		return age;
	}
	public int salary()
	{
		return salary;
	}
	public int joindate()
	{
		return joindate;
	}
	
}