//Single-Responsibility


interface sum
{
	public void sum(int x,int y);
}
class sum1 implements sum
{
	public void sum(int x,int y)
	{
		System.out.println(x+y);
	}
}

//Open-Closed

interface calc
{
	public void calc(int x,int y);
}
class sum2 implements calc
{
	public void calc(int x,int y)
	{
		System.out.println(x+y);
	}
}
class multi implements calc
{
	public void calc(int x,int y)
	{
		System.out.println(x*y);
	}
}
class divide implements calc
{
	public void calc(int x,int y)
	{
		System.out.println(x/y);
	}
}

//Interface Segregation

interface animals
{
	public void feed();
}
interface humans extends animals
{
	public void cloths();
	public void job();
}

class dog implements animals
{
	public void feed()
	{
		System.out.println("bhook lagegi toh bark krega");
	}
}
class shub implements humans
{
	public void feed()
	{
		System.out.println("bhook lagegi toh khud khaa lega");
	}
	public void cloths()
	{
		System.out.println("Formal chaiye");
	}
	public void job()
	{
		System.out.println("Innogent me krta h");
	}
}

class q19
{
	public static void main(String ar[])
	{
		//Single-Responsibility
        // sum1 s=new sum1();
		// s.sum(10,20);
		
		
		//Open-Closed
		// sum2 s1=new sum2();
		// s1.calc(10,5);
		
		// multi m1=new multi();
		// m1.calc(10,5);
		
		// divide d1=new divide();
		// d1.calc(10,5);
		
		
		//Interface Segregation
		dog d=new dog();
		d.feed();
		
		shub s=new shub();
		s.feed();
		s.cloths();
		s.job();

	}
}