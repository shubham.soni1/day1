class Encapsulation
{
	private int radius;
	private int area;
	
	public void setradius(int radius)
	{
		this.radius=radius;
	}
	
	public int radius()
	{
		return radius;
	}
	public void setarea(int area)
	{
		this.area=area;
	}
	
	public int area()
	{
		return area;
	}
}

class q78
{
	public static void main(String aar[])
	{
		Encapsulation a=new Encapsulation();
		a.setradius(10);
		a.setarea(20);
		System.out.println(a.radius());
		System.out.println(a.area());
	}
}