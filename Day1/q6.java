class StaticOverLoading
{
	static void show(int x)
	{
		System.out.println(x);
	}
	static void show(int x,int y)
	{
		System.out.println(x+y);
	}
}

class  q6
{
	public static void main(String ar[])
	{
		StaticOverLoading a=new StaticOverLoading();
		a.show(10);
		a.show(10,20);
	}
}