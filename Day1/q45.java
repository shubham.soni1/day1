class OverLoading
{
	void show(int x)
	{
		System.out.println(x);
	}
	void show(int x,int y)
	{
		System.out.println(x+y);
	}void show(int x,int y,int z)
	{
		System.out.println(x+y+z);
	}
}
class Overridng extends OverLoading
{
	void show(int x)
	{
		System.out.println("Class A");
	}
}


class q45
{
	public static void main(String ar[])
	{
		OverLoading a=new OverLoading();
		OverLoading.show(10);
		OverLoading.show(10,20);
		OverLoading.show(10,20,30);
		
		Overridng overriding=new Overridng();
		overriding.show(1);
	}
}