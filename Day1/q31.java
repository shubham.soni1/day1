import java.util.*;

class q31
{
	public static void main(String ar[])
	{
		ArrayList al=new ArrayList();
		
		organization o1=new organization("shub","male","Developer",28,10000,2011);
		organization o2=new organization("shivi","female","Tester",26,20000,2022);
		organization o3=new organization("kuldeep","male","FrontEnd",30,30000,2019);
		organization o4=new organization("nandani","female","BackEnd",20,25000,2018);
		organization o5=new organization("atual","male","sales and markiting",23,28000,2013);
		organization o6=new organization("shub1","male","Developer",18,10000,2011);
		organization o7=new organization("gunjiii","female","sales and markiting",23,28000,2013);
		organization o8=new organization("mainka","female","sales and markiting",33,28000,2013);
		organization o9=new organization("rohan ","male","sales and markiting",33,28000,2013);
		organization o10=new organization("rohani ","female","sales and markiting",33,28000,2013);
		
		al.add(o1);
		al.add(o2);
		al.add(o3);
		al.add(o4);
		al.add(o5);
		al.add(o6);
		al.add(o7);
		al.add(o8);
		al.add(o9);
		al.add(o10);
		// q31.Gendercount(al);
		// q31.Department(al);
		// q31.Avrage(al);
		// q31.highestsalary(al);
		// q31.joinAfter2015(al);
		// q31.empDepartment(al);
		// q31.SalDepartment(al);
		// q31.youngemp(al);
		// q31.malefemale(al);
		// q31.salesmarkiting(al);
		// q31.empDepartmentwise(al);
		// q31.avgtotal(al);
		// q31.young25(al);
		q31.oldest(al);
	}
	
	static void oldest(ArrayList al)
	{
		int max=0;
		String name="";
		for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			if(max<o.age())
			{
				name=o.name();
				max=o.age();
			}
		}
		System.out.println("Name = "+name);
		System.out.println("Age = "+max);
		
		
	}
	
	static void  young25(ArrayList al)
	{
		
		ArrayList young=new ArrayList();
		
		for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			if(o.age()<=25)
			{
				young.add(al.get(i));
			}
		}
		System.out.println(young);
		
		
	}
	
	static void avgtotal(ArrayList al)
	{
		int avg=0;
		int total=0,count=0;
		for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			avg+=o.salary();
			total+=o.salary();
			count++;
		}
		System.out.println("Avarage salary"+(avg/count));
		System.out.println("total salary"+(total));
		
	}
	
	static void empDepartmentwise(ArrayList al)
	{
		
		ArrayList developer=new ArrayList();
		ArrayList tester=new ArrayList();
		ArrayList frontend=new ArrayList();
		ArrayList backend=new ArrayList();
		ArrayList salesmarkiting=new ArrayList();
		
		for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			if((o.department()).equals("Tester"))
			{
				developer.add(al.get(i));
			}
			if((o.department()).equals("FrontEnd"))
			{
				frontend.add(al.get(i));
			}
			if((o.department()).equals("BackEnd"))
			{
				backend.add(al.get(i));
			}
			if((o.department()).equals("Developer"))
			{
				salesmarkiting.add(al.get(i));
			}
		}
		
		System.out.println(developer);
		System.out.println(frontend);
		System.out.println(backend);
		System.out.println(salesmarkiting);
		
	}
	
	static void salesmarkiting(ArrayList al)
	{
		int male=0;
		int female=0;
		
		for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			if(o.department().equals("sales and markiting"))
			{
			if(o.gender().equals("male"))
			{
				male++;
			}
			if(o.gender().equals("female"))
			{
				female++;
			}
			}
		}
		System.out.println("male  = "+male);
		System.out.println("Female = "+female);
	
	}
	
	static void malefemale(ArrayList al)
	{
		
		int male=0;
		int malesal=0;
		int female=0;
		int femalesal=0;
		
		for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			if(o.gender().equals("male"))
			{
				malesal+=o.salary();
				male++;
			}
			if(o.gender().equals("female"))
			{
				femalesal+=o.salary();
				female++;
			}
		}
		System.out.println("Average male Salary = "+(malesal/male));
		System.out.println("Average Female Salary = "+(femalesal/female));
	}
	
	static void youngemp(ArrayList al)
	{
		String name="";
		int min=0;
		for(int i=0;i<al.size();i++)
		{
		organization o=(organization)al.get(i);
		if(i==0){min=o.age();}
		 if(o.department().equals("Developer"))
		{
			if(min>=o.age())
			{
				System.out.println("2");
				name=o.name();
				min=o.age();
			}
			
		}	
		
		}
		System.out.println("Name = "+ name);
		System.out.println("Age = "+ min);
		
	}
	
	static void SalDepartment(ArrayList al)
    {
		HashMap<String ,Integer> h=new HashMap<>();
		int count=0;
		int tester=0;
		int frontend=0;
		int backend=0;
		int developer=0;
		
		for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			if((o.department()).equals("Tester"))tester+=o.salary();
			if((o.department()).equals("FrontEnd"))frontend+=o.salary();
			if((o.department()).equals("BackEnd"))backend+=o.salary();
			if((o.department()).equals("Developer"))developer+=o.salary();
		}		
		
System.out.println("Dep Tester = "+tester);		
System.out.println("dep FrontEnd"+frontend);		
System.out.println("dep BackEnd"+backend);		
System.out.println("dep developer"+developer);		
	}
	
	static void empDepartment(ArrayList al)
    {
		HashMap<String ,Integer> h=new HashMap<>();
		int count=0;
		for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			if( h.containsKey(o.department()))
			h.put(o.department(),count+1);
		else
			h.put(o.department(),count);
			
		}		
System.out.println(h);		
	}
	
	
	
	static void joinAfter2015(ArrayList al)
	{
		
		for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			if(o.joindate()>2015)System.out.println(o.name());
		}
	}
	
	static void highestsalary(ArrayList al)
	{
		int max=0;
		String name="";
		for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			if(o.salary()>max)
			{
				name=o.name();
				max=o.salary();
			}
		}
		System.out.println("Name = "+name);
		System.out.println("Highest Salary = "+max);
	}
	
	static void Gendercount(ArrayList al)
	 {
		
		int male=0;
		int female=0;
		
		for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			if(o.gender().equals("male"))
			{
				male++;
			}
			if(o.gender().equals("female"))
			{
				female++;
			}
		}
		System.out.println(male);
		System.out.println(female);
	}
	static void Department(ArrayList al)
	{
		HashSet h=new HashSet();
	
			
			for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			if(h.contains(o.department()))
			{}
			else
			{
				h.add(o.department());
			}
		}
			System.out.println(h);
		
	}
	
	static void Avrage(ArrayList al)
	{
		int male=0;
		int maleage=0;
		int female=0;
		int femaleage=0;
		
		for(int i=0;i<al.size();i++)
		{
			organization o=(organization)al.get(i);
			if(o.gender().equals("male"))
			{
				maleage+=o.age();
				male++;
			}
			if(o.gender().equals("female"))
			{
				femaleage+=o.age();
				female++;
			}
		}
		System.out.println("Average Male Age = "+(maleage/male));
		System.out.println("Average Female Age = "+(femaleage/female));

	}
	
}